package com.blog.clients;

import com.blog.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/22 17:17
 * @Info :
 */

@FeignClient("userservice")
public interface UserClient {
    @GetMapping("/user/{id}")
    public User getUserById(Long id);
}
