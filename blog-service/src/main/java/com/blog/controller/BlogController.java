package com.blog.controller;

import com.blog.pojo.Blog;
import com.blog.pojo.User;
import com.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:31
 * @Info :
 */

@RestController
public class BlogController {

    @Autowired
    private BlogService blogService;

    @GetMapping("/blog/list")
    public List<Blog> getBlogAll(){
        return blogService.getBlogAll();
    }

    @GetMapping("/blog/{id}")
    public Blog getUserById(@PathVariable("id") Long id){
        return blogService.getBlogById(id);
    }

}
