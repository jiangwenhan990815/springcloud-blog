package com.blog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:41
 * @Info :
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Blog implements Serializable {

    private String name;
    private long userId;

    private User user;

}
