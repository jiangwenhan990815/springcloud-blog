package com.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:20
 * @Info :
 */

@EnableFeignClients
@SpringBootApplication
public class BlogBootApplicatiion {
    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled","false");
        SpringApplication.run(BlogBootApplicatiion.class, args);
    }
}
