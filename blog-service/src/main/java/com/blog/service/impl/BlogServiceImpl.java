package com.blog.service.impl;

import com.blog.clients.UserClient;
import com.blog.mapper.BlogMapper;
import com.blog.pojo.Blog;
import com.blog.pojo.User;
import com.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:30
 * @Info :
 */

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogMapper blogMapper;
    @Autowired
    private RestTemplate restTemplate;
//    @Value("${service-url.user-service-url}")
//    public String serviceUrl;
    @Autowired
    private UserClient userClient;

    @Override
    public List<Blog> getBlogAll() {

        List<Blog> blogAll = blogMapper.getBlogAll();

        for (Blog blog: blogAll){
            User user = userClient.getUserById(blog.getUserId());

            blog.setUser(user);
        }

        return blogAll;
    }

    @Override
    public Blog getBlogById(Long id) {
        return blogMapper.getBlogById(id);
    }
}
