package com.blog.service;

import com.blog.pojo.Blog;
import com.blog.pojo.User;

import java.util.List;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:30
 * @Info :
 */


public interface BlogService {

    // 获取用户全部信息
    public List<Blog> getBlogAll();

    // 根据id获取用户信息
    public Blog getBlogById(Long id);

}
