package com.blog;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 16:43
 * @Info :
 */

@SpringBootTest
public class BlogBootApplicationTest {

    @Test
    public void getHashMap(){
        HashMap<String, Object> map = new HashMap<>();

        map.put("jiang", 1);
        map.put("wen", 1);
        map.put("han", 1);
    }

}
