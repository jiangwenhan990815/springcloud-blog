package com.blog.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: jiangwenhan
 * @Date: 2024/2/27 15:34
 * @Info :
 */
@RestController
public class TestController {
    @GetMapping("get")
    public String get() {
        return "喵喵喵";
    }
}
