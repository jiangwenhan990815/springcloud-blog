package com.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author: jiangwenhan
 * @Date: 2024/2/27 15:03
 * @Info :
 */

@SpringBootApplication
//@EnableDiscoveryClient
public class NacosApplication {
    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled","false");
        SpringApplication.run(NacosApplication.class, args);
    }
}
