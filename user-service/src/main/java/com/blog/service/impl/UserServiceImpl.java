package com.blog.service.impl;

import com.blog.mapper.UserMapper;
import com.blog.pojo.User;
import com.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:30
 * @Info :
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> getUserAll() {
        return userMapper.getUserAll();
    }

    @Override
    public User getUserById(Long id) {
        return userMapper.getUserById(id);
    }
}
