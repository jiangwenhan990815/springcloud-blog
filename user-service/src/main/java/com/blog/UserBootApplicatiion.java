package com.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:20
 * @Info :
 */

@SpringBootApplication
public class UserBootApplicatiion {
    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled","false");
        SpringApplication.run(UserBootApplicatiion.class, args);
    }
}
