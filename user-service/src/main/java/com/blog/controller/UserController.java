package com.blog.controller;

import com.blog.config.PatternProperties;
import com.blog.pojo.User;
import com.blog.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:31
 * @Info :
 */

@RestController
//@RefreshScope
public class UserController {

    @Autowired
    private UserService userService;
//    @Value("${pattern.dateformat}")
//    private String dataformat;
    @Autowired
    private PatternProperties properties;

    @GetMapping("/now")
    public String getConfig(){
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(properties.getDateformat()));
    }

    @GetMapping("/user/list")
    public List<User> getUserAll(){
        return userService.getUserAll();
    }

    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable("id") Long id){
        return userService.getUserById(id);
    }

}
