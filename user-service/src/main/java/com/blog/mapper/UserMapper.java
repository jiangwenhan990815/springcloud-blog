package com.blog.mapper;

import com.blog.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: jiangwenhan
 * @Date: 2023/2/21 05:27
 * @Info :
 */

@Mapper
@Repository
public interface UserMapper {

    // 获取用户全部信息
    public List<User> getUserAll();

    // 根据id获取用户信息
    public User getUserById(Long id);

}
